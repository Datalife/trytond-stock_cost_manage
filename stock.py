# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.modules.cost_manage import BasicCostSource

__all__ = ['Move']

__metaclass__ = PoolMeta


class Move(BasicCostSource):

    __name__ = 'stock.move'

    @fields.depends('effective_date')
    def on_change_with_cost_date(self, field=None):
        return getattr(self, 'effective_date', None)

    @fields.depends('work')
    def on_change_with_currency_digits(self, name=None):
        if self.company:
            return self.company.currency.digits
        return 2

    @fields.depends('quantity', 'internal_quantity', 'date')
    def on_change_with_cost_amount(self, name=None):
        if self.state == 'done':
            return Decimal(str(self.internal_quantity)) * self.cost_price
        else:
            return 0

    def get_rec_name(self, name):
        rec_name = super(Move, self).get_rec_name(name)
        if self.production_input:
            rec_name += ',' + self.production_input.get_rec_name(name)
        return rec_name

    def get_cost_type(self, name=None):
        return 'cost'

    def get_cost_family(self, name=None):
        if self.product.category:
            return self.product.category.name
        else:
            return self.product.name
